//
//  SecondViewController.swift
//  Speechless
//
//  Created by Fabbio on 17/12/2019.
//  Copyright © 2019 Fabbio. All rights reserved.
//

import UIKit


class FirstViewController: UIViewController {
    

    @IBOutlet weak var image: UnsplashImageView!
    @IBOutlet weak var startButton: UIButton!
    @IBAction func startButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func endButton(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated:true);
        navigationController?.navigationBar.barStyle = .black
        self.navigationItem.title = expertLabelFull
        image.load(url: URL(string: "https://source.unsplash.com/random")!)
    }
    override func viewDidAppear(_ animated: Bool) {
//        image.fetchPhoto()
        UIApplication.shared.isIdleTimerDisabled = true

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
    }

}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
