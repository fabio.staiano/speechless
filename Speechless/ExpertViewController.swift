//
//  ExpertViewController.swift
//  Speechless
//
//  Created by Fabbio on 17/12/2019.
//  Copyright © 2019 Fabbio. All rights reserved.
//

import UIKit

var expertLabelFull = " "

class ExpertViewController: UIViewController {
    @IBOutlet weak var expertLabel: UILabel!
    
    var expertAdjectives = ["attractive", "beautiful", "clean", "elegant", "fancy", "fit", "glamorous", "handsome", "long", "magnificent", "plain", "short", "skinny", "ugly", "alive","better", "clever", "dead", "easy", "famous", "gifted", "helpful", "important", "inexpensive", "poor","powerful","rich","shy","specific","wrong","old","aggressive","ambitious","brave","calm","delightful","faithful","gentle","happy","jolly","kind","nice","obedient","polite","proud","silly","thankful","wonderful","angry","embarrassed","grumpy","helpless","jealous","lazy","mysterious","nervous","scary","worried","curved","deep","flat","high","low","rounded","squared","wide","big","colossal","fat","gigantic","great","huge","large","little","massive","microscopic","short","small","tall","tiny","crashing","loudy","noisy","rhythmic","whispering","ancient","fast","future","late","long","modern","old","old-fashioned","prehistoric","quick","rapid","short","slow","young","cool","creamy","delicious","disgusting","fresh","juicy","hot","nutritious","putrid","spicy","sweet","tasteless","tasty","chilly","cold","damaged","dirty","dry","freezing","icy","melted","sharp","slimy","sticky","strong","warm","weak","wet","wooden","3d-printed","fake"].shuffled()
    
    var expertTitles = ["rockets", "socks", "snakes", "gnomes","porn","giraffes","songs","magic tricks","clothes","french fries","bananas","pizzas","cars","ufos","fast foods","memes", "cats pics", "childs","horses", "kangaroos", "puppies","trash","shit","medicine","balls","genitals","penis","farts","bazookas","aliens","doors","sofas","monkeys","dogs","elephants","mens","womens","girls", "boys","bicycles","sheriffs","movies","cocktails","drugs","guns","americans","apps","tv series","terrorists","instruments","dragons","ghosts","rats","koalas","pigs","tits","nerds","stormtroopers","boy scouts","virgins","volcanos","objects","demons","shapeshifters","condoms","vampires","zombies","bears","grandmas","asians","goblins","dungeons","heroes","cakes","gods","jokes"].shuffled()
    
    
    func getTitle() {
        expertLabel.text = expertAdjectives[1] + " " + expertTitles[1]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem?.tintColor = UIColor.black

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        expertLabelFull = " "
        getTitle()
        expertLabelFull = "\(expertLabel.text!)"
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
